package com.example.demo.respository.impl;

import com.example.demo.dto.SudokuDto;
import com.example.demo.respository.SudokuRespo;
import com.opencsv.CSVReader;
import org.springframework.stereotype.Repository;



import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class SudokuRespoImpl implements SudokuRespo {
    private SudokuDto board;
    private static final String FILE = "sudoku.csv";

    public SudokuRespoImpl() {
        createCsvFile();
    }

    public void createCsvFile(){
        List<List<String>> recordsFromFile = new ArrayList<>();
        try(CSVReader csvReader = new CSVReader(new FileReader(FILE))){
            String[] values;
            while((values = csvReader.readNext()) != null){
                recordsFromFile.add(Arrays.asList(values));
            }
        } catch( FileNotFoundException e){} catch (IOException e) {
            e.printStackTrace();
        }

        convertToNumbers(recordsFromFile);
    }

    public void convertToNumbers(List<List<String>> recordsFromFile){
        List<List<Integer>> boardValues = new ArrayList<>();
        for(List<String> list:recordsFromFile){
            List<Integer> holder = new ArrayList<>();
            for(String item:list){
                holder.add(Integer.parseInt(item));
            }
            boardValues.add(holder);
        }
        board = new SudokuDto(boardValues);
    }

    @Override
    public SudokuDto getBoard() {
        return board;
    }
}
