package com.example.demo.respository;

import com.example.demo.dto.SudokuDto;
import org.springframework.stereotype.Service;


@Service
public interface SudokuRespo {
    void createCsvFile();
    SudokuDto getBoard();
}
