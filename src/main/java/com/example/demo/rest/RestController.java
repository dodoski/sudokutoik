package com.example.demo.rest;

import com.example.demo.service.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/sudoku")
public class RestController {

    @Autowired
    SudokuService sudokuService;


    @PostMapping(value = "verify")
    public ResponseEntity verify(){
        if(sudokuService.verifySudoku()){
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(sudokuService.getErrorDto(), HttpStatus.BAD_REQUEST);
    }


}
