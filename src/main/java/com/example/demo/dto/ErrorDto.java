package com.example.demo.dto;

import java.util.List;
import java.util.ListIterator;

public class ErrorDto {

    private List<Integer> lineIds;
    private List<Integer> columnIds;
    private List<Integer> areaIds;

    public ErrorDto(List<Integer> line, List<Integer> column, List<Integer> area) {
        this.lineIds = line;
        this.columnIds = column;
        this.areaIds = area;
    }

    public ErrorDto() {
    }

    public List<Integer> getLine() {
        return lineIds;
    }

    public List<Integer> getColumn() {
        return columnIds;
    }

    public void setLine(List<Integer> line) {
        this.lineIds = lineIds;
    }

    public void setColumn(List<Integer> column) {
        this.columnIds = columnIds;
    }

    public void setArea(List<Integer> area) {
        this.areaIds = areaIds;
    }

    public List<Integer> getArea() {
        return areaIds;
    }
}
