package com.example.demo.dto;

import java.util.List;

public class SudokuDto {

    public void setBoard(List<List<Integer>> board) {
        this.board = board;
    }

    public List<List<Integer>> getBoard() {
        return board;
    }

    private List<List<Integer>> board;

    public SudokuDto(){ }

    public SudokuDto(List<List<Integer>> board){
        this.board = board;
    }

}
