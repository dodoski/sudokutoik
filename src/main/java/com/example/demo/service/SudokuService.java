package com.example.demo.service;

import com.example.demo.dto.ErrorDto;
import com.example.demo.dto.SudokuDto;


public interface SudokuService {
    SudokuDto getBoard();
    boolean verifySudoku();
    ErrorDto getErrorDto();
}
